import kotlin.math.sqrt

class Point(private val x: Int = 0, private val y: Int = 0){

    override fun equals(other: Any?): Boolean {
        return (other is Point) && (this.x == other.x) && (this.y == other.y)
    }

    override fun toString(): String {
        val x = x.toString()
        val y = y.toString()
        return "$x $y"
    }

    override fun hashCode(): Int {
        val result = 31 * result + y
        return result
    }

    fun reverse(): Point{
        return Point(-x, -y)
    }

    fun distance(p0: Point): Double {
        val distance = sqrt((p0.x - x) * (p0.x - x) + (p0.y - y) * (p0.y - y).toDouble())

        return distance
    }
}